package pl.klolo.workshops.logic;

import pl.klolo.workshops.domain.*;
import pl.klolo.workshops.domain.Currency;
import pl.klolo.workshops.mock.HoldingMockGenerator;
import pl.klolo.workshops.mock.UserMockGenerator;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.naming.OperationNotSupportedException;

import static java.lang.String.format;
import static java.util.stream.Collectors.*;
import static java.util.stream.Collectors.toSet;

class WorkShop {
    /**
     * Lista holdingów wczytana z mocka.
     */
    private final List<Holding> holdings;

    private final Predicate<User> isWoman = user -> user.getSex().equals(Sex.WOMAN);
    private Predicate<User> isMan = m -> m.getSex() == Sex.MAN;

    WorkShop() {
        final HoldingMockGenerator holdingMockGenerator = new HoldingMockGenerator();
        holdings = holdingMockGenerator.generate();
    }

    /**
     * Metoda zwraca liczbę holdingów w których jest przynajmniej jedna firma.
     */
    long getHoldingsWhereAreCompanies() {
    	return holdings.stream()
    		.filter(i -> i.getCompanies().size() > 1)
    		.count();
    }

    /**
     * Zwraca nazwy wszystkich holdingów pisane z małej litery w formie listy.
     */
    List<String> getHoldingNames() {
    	return holdings.stream()
    		.map(Holding::getName)
    		.map(String::toLowerCase)
    		.collect(Collectors.toList());
    }

    /**
     * Zwraca nazwy wszystkich holdingów sklejone w jeden string i posortowane.
     * String ma postać: (Coca-Cola, Nestle, Pepsico)
     */
    String getHoldingNamesAsString() {
    	return holdings.stream()
    		.map(Holding::getName)
    		.sorted()
    		.collect(Collectors.joining(", ", "(", ")"));
    }

    /**
     * Zwraca liczbę firm we wszystkich holdingach.
     */
    long getCompaniesAmount() {
    	return holdings.stream()
    		.flatMap(i -> i.getCompanies().stream())
    		.count();
    }

    /**
     * Zwraca liczbę wszystkich pracowników we wszystkich firmach.
     */
    long getAllUserAmount() {
    	return holdings.stream()
    		.flatMap(i -> i.getCompanies().stream())
    		.flatMap(i -> i.getUsers().stream())
    		.count();
    }

    /**
     * Zwraca listę wszystkich nazw firm w formie listy. Tworzenie strumienia firm umieść w osobnej metodzie którą
     * później będziesz wykorzystywać.
     */
    List<String> getAllCompaniesNames() {
    	return getCompanies()
    		.map(Company::getName)
    		.collect(Collectors.toList());
    }
    
    private Stream<Company> getCompanies() {
    	return holdings.stream().
    		flatMap(i -> i.getCompanies().stream());
    }

    /**
     * Zwraca listę wszystkich firm jako listę, której implementacja to LinkedList. Obiektów nie przepisujemy
     * po zakończeniu działania strumienia.
     */
    LinkedList<String> getAllCompaniesNamesAsLinkedList() {
//    	return getCompanies()
//    			.map(Company::getName)
//    			.collect(Collector.of(() -> new LinkedList<String>(), 
//    					LinkedList::add, 
//    					(r1, r2) -> {
//    						r1.addAll(r2);
//    						return r1;
//    					}));
    	
    	return getCompanies()
    			.map(Company::getName)
    			.collect(Collectors.toCollection(LinkedList::new));
    
    }

    /**
     * Zwraca listę firm jako String gdzie poszczególne firmy są oddzielone od siebie znakiem "+"
     */
    String getAllCompaniesNamesAsString() {
    	return getCompanies()
    		.map(Company::getName)
    		.collect(Collectors.joining("+"));
    }

    /**
     * Zwraca listę firm jako string gdzie poszczególne firmy są oddzielone od siebie znakiem "+".
     * Używamy collect i StringBuilder.
     * <p>
     * UWAGA: Zadanie z gwiazdką. Nie używamy zmiennych.
     */
    String getAllCompaniesNamesAsStringUsingStringBuilder() {
    	return getCompanies()
    		.map(Company::getName)
    		.collect(Collector.of(() -> new StringBuilder(), 
    				(result, nextName) -> result.append(nextName).append("+"), 
    				(r1, r2) -> r1.append(r2), 
    				sb -> sb.toString().substring(0, sb.toString().length() - 1)));
    }

    /**
     * Zwraca liczbę wszystkich rachunków, użytkowników we wszystkich firmach.
     */
    long getAllUserAccountsAmount() {
    	return getCompanies()
    		.flatMap(i -> i.getUsers().stream())
    		.flatMap(i -> i.getAccounts().stream())
    		.count();
    }

    /**
     * Zwraca listę wszystkich walut w jakich są rachunki jako string, w którym wartości
     * występują bez powtórzeń i są posortowane.
     */
    String getAllCurrencies() {
    	return getCompanies()
    		.flatMap(i -> i.getUsers().stream())
    		.flatMap(i -> i.getAccounts().stream())
    		.map(Account::getCurrency)
    		.distinct()
    		.map(Currency::toString)
    		.sorted()
    		.collect(Collectors.joining(", "));
    }

    /**
     * Metoda zwraca analogiczne dane jak getAllCurrencies, jednak na utworzonym zbiorze nie uruchamiaj metody
     * stream, tylko skorzystaj z  Stream.generate. Wspólny kod wynieś do osobnej metody.
     *
     * @see #getAllCurrencies()
     */
    String getAllCurrenciesUsingGenerate() {
    	throw new RuntimeException(new OperationNotSupportedException());
    }

    private List<String> getAllCurrenciesToListAsString() {
    	throw new RuntimeException(new OperationNotSupportedException());
    }

    /**
     * Zwraca liczbę kobiet we wszystkich firmach. Powtarzający się fragment kodu tworzący strumień użytkowników umieść
     * w osobnej metodzie. Predicate określający czy mamy do czynienia z kobietą niech będzie polem statycznym w klasie.
     */
    long getWomanAmount() {
    	return holdings.stream()
    			.flatMap(i -> i.getCompanies().stream())
    			.flatMap(i -> i.getUsers().stream())
    			.filter(i -> i.getFirstName().endsWith("a"))
    			.count();
    }
    
    /**
     * Przelicza kwotę na rachunku na złotówki za pomocą kursu określonego w enum Currency.
     */
    BigDecimal getAccountAmountInPLN(final Account account) {
    	return account.getAmount()
    			.multiply(BigDecimal.valueOf(account.getCurrency().rate), new MathContext(4, RoundingMode.HALF_UP));
    }

    /**
     * Przelicza kwotę na podanych rachunkach na złotówki za pomocą kursu określonego w enum Currency i sumuje ją.
     */
    BigDecimal getTotalCashInPLN(final List<Account> accounts) {
    	return accounts.stream()
    			.map(this::getAccountAmountInPLN)
    			.reduce(BigDecimal::add)
    			.get();
    }

    private Stream<User> allUsers() {
    	return holdings.stream()
    			.flatMap(i -> i.getCompanies().stream())
    			.flatMap(i -> i.getUsers().stream());
    }
    
    /**
     * Zwraca imiona użytkowników w formie zbioru, którzy spełniają podany warunek.
     */
    Set<String> getUsersForPredicate(final Predicate<User> userPredicate) {
    	return allUsers()
    			.filter(userPredicate)
    			.map(User::getFirstName)
    			.collect(Collectors.toSet());
    }

    /**
     * Metoda filtruje użytkowników starszych niż podany jako parametr wiek, wyświetla ich na konsoli, odrzuca mężczyzn
     * i zwraca ich imiona w formie listy.
     */
    List<String> getOldWoman(final int age) {
    	return allUsers()
    			.filter(i -> i.getAge() > 50)
    			.filter(i -> !i.getFirstName().endsWith("a"))
    			.map(User::getFirstName)
    			.collect(Collectors.toList());
    }

    /**
     * Dla każdej firmy uruchamia przekazaną metodę.
     */
    void executeForEachCompany(final Consumer<Company> consumer) {
    	getCompanies()
    		.forEach(consumer);
    }

    /**
     * Wyszukuje najbogatsza kobietę i zwraca ją. Metoda musi uzwględniać to że rachunki są w różnych walutach.
     */
    //pomoc w rozwiązaniu problemu w zadaniu: https://stackoverflow.com/a/55052733/9360524
    Optional<User> getRichestWoman() {
    	return allUsers()
    			.filter(isWoman)
    			.sorted(Comparator.comparing(this::getUserAmountInPLN).reversed())
    			.findFirst();
    }

    private BigDecimal getUserAmountInPLN(final User user) {
    	return user.getAccounts().stream()
    		.map(this::getAccountAmountInPLN)
    		.reduce(BigDecimal::add)
    		.orElse(BigDecimal.ZERO);
    }

    /**
     * Zwraca nazwy pierwszych N firm. Kolejność nie ma znaczenia.
     */
    Set<String> getFirstNCompany(final int n) {
    	return holdings.stream()
    			.flatMap(i -> i.getCompanies().stream())
    			.limit(5)
    			.map(Company::getName)
    			.collect(Collectors.toSet());
    }
    
    private Stream<Account> allAccounts() {
    	return allUsers()
    			.flatMap(i -> i.getAccounts().stream());
    }

    /**
     * Metoda zwraca jaki rodzaj rachunku jest najpopularniejszy. Stwórz pomocniczą metodę getAccountStream.
     * Jeżeli nie udało się znaleźć najpopularniejszego rachunku metoda ma wyrzucić wyjątek IllegalStateException.
     * Pierwsza instrukcja metody to return.
     */
    AccountType getMostPopularAccountType() {
    	return allAccounts()
    			.collect(Collectors.groupingBy(Account::getType))
    			.entrySet()
    			.stream()
    			.map(i -> i.getValue())
    			.sorted(Comparator.comparing(List::size, Comparator.reverseOrder()))
    			.findFirst()
    			.orElseThrow(IllegalStateException::new)
    			.stream()
    			.findFirst()
    			.orElseThrow(IllegalStateException::new)
    			.getType();
    }

    /**
     * Zwraca pierwszego z brzegu użytkownika dla podanego warunku. W przypadku kiedy nie znajdzie użytkownika wyrzuca
     * wyjątek IllegalArgumentException.
     */
    User getUser(final Predicate<User> predicate) {
    	return allUsers()
    			.filter(predicate)
    			.findFirst()
    			.orElseThrow(IllegalArgumentException::new);
    }

    /**
     * Zwraca mapę firm, gdzie kluczem jest jej nazwa a wartością lista pracowników.
     */
    Map<String, List<User>> getUserPerCompany() {
    	return this.getCompanies()
    			.collect(Collectors.toMap(Company::getName, Company::getUsers));
    }


    /**
     * Zwraca mapę firm, gdzie kluczem jest jej nazwa a wartością lista pracowników przechowywanych jako String
     * składający się z imienia i nazwiska. Podpowiedź:  Możesz skorzystać z metody entrySet.
     */
    Map<String, List<String>> getUserPerCompanyAsString() {
    	return getCompanies()
    		.collect(Collectors.toMap(Company::getName, 
    				i -> i.getUsers().stream()
    					.map(user -> user.getFirstName()+" "+user.getLastName())
    					.collect(Collectors.toList())));
    }

    /**
     * Zwraca mapę firm, gdzie kluczem jest jej nazwa a wartością lista pracowników przechowywanych jako obiekty
     * typu T, tworzonych za pomocą przekazanej funkcji.
     */
    //pomoc w rozwiązaniu problemu w zadaniu: https://stackoverflow.com/a/54969615/9360524
    <T> Map<String, List<T>> getUserPerCompany(final Function<User, T> converter) {
    	return getCompanies()
    			.collect(Collectors.toMap(Company::getName, i -> i.getUsers()
    					.stream()
    					.map(converter)
    					.collect(Collectors.toList())));
    }

    /**
     * Zwraca mapę gdzie kluczem jest flaga mówiąca o tym czy mamy do czynienia z mężczyzną, czy z kobietą.
     * Osoby "innej" płci mają zostać zignorowane. Wartością jest natomiast zbiór nazwisk tych osób.
     */
    Map<Boolean, Set<String>> getUserBySex() {
    	throw new RuntimeException(new OperationNotSupportedException());
    }

    /**
     * Zwraca mapę rachunków, gdzie kluczem jest numer rachunku, a wartością ten rachunek.
     */
    Map<String, Account> createAccountsMap() {
    	return allAccounts()
    			.collect(Collectors.toMap(Account::getNumber, Function.identity()));
    }

    /**
     * Zwraca listę wszystkich imion w postaci Stringa, gdzie imiona oddzielone są spacją i nie zawierają powtórzeń.
     */
    String getUserNames() {
    	return allUsers()
    			.map(User::getFirstName)
    			.distinct()
    			.sorted()
    			.collect(Collectors.joining(" "));
    }

    /**
     * Zwraca zbiór wszystkich użytkowników. Jeżeli jest ich więcej niż 10 to obcina ich ilość do 10.
     */
    Set<User> getUsers() {
    	return allUsers()
    			.limit(10l)
    			.collect(Collectors.toSet());
    }

    /**
     * Zapisuje listę numerów rachunków w pliku na dysku, gdzie w każda linijka wygląda następująco:
     * NUMER_RACHUNKU|KWOTA|WALUTA
     * <p>
     * Skorzystaj z strumieni i try-resources.
     */
    void saveAccountsInFile(final String fileName) {
    	throw new RuntimeException(new OperationNotSupportedException());
    }

    /**
     * Zwraca użytkownika, który spełnia podany warunek.
     */
    Optional<User> findUser(final Predicate<User> userPredicate) {
    	return allUsers()
    			.filter(userPredicate)
    			.findFirst();
    }

    /**
     * Dla podanego użytkownika zwraca informacje o tym ile ma lat w formie:
     * IMIE NAZWISKO ma lat X. Jeżeli użytkownik nie istnieje to zwraca text: Brak użytkownika.
     * <p>
     * Uwaga: W prawdziwym kodzie nie przekazuj Optionali jako parametrów.
     */
    String getAdultantStatus(final Optional<User> user) {
    	throw new RuntimeException(new OperationNotSupportedException());
    }

    /**
     * Metoda wypisuje na ekranie wszystkich użytkowników (imię, nazwisko) posortowanych od z do a.
     * Zosia Psikuta, Zenon Kucowski, Zenek Jawowy ... Alfred Pasibrzuch, Adam Wojcik
     */
    void showAllUser() {
    	throw new RuntimeException(new OperationNotSupportedException());
    }

    /**
     * Zwraca mapę, gdzie kluczem jest typ rachunku a wartością kwota wszystkich środków na rachunkach tego typu
     * przeliczona na złotówki.
     */
    //TODO: fix
    // java.lang.AssertionError:
    // Expected :87461.4992
    // Actual   :87461.3999
    Map<AccountType, BigDecimal> getMoneyOnAccounts() {
    	throw new RuntimeException(new OperationNotSupportedException());
    }

    /**
     * Zwraca sumę kwadratów wieków wszystkich użytkowników.
     */
    int getAgeSquaresSum() {
    	throw new RuntimeException(new OperationNotSupportedException());
    }

    /**
     * Metoda zwraca N losowych użytkowników (liczba jest stała). Skorzystaj z metody generate. Użytkownicy nie mogą się
     * powtarzać, wszystkie zmienną muszą być final. Jeżeli podano liczbę większą niż liczba użytkowników należy
     * wyrzucić wyjątek (bez zmiany sygnatury metody).
     */
    List<User> getRandomUsers(final int n) {
    	throw new RuntimeException(new OperationNotSupportedException());
    }

    /**
     * Zwraca strumień wszystkich firm.
     */
    private Stream<Company> getCompanyStream() {
    	throw new RuntimeException(new OperationNotSupportedException());
    }

    /**
     * Zwraca zbiór walut w jakich są rachunki.
     */
    private Set<Currency> getCurenciesSet() {
    	throw new RuntimeException(new OperationNotSupportedException());
    }

    /**
     * Tworzy strumień rachunków.
     */
    private Stream<Account> getAccoutStream() {
    	return holdings.stream()
    			.flatMap(i -> i.getCompanies().stream())
    			.flatMap(i -> i.getUsers().stream())
    			.flatMap(i -> i.getAccounts().stream());
    }

    /**
     * Tworzy strumień użytkowników.
     */
    private Stream<User> getUserStream() {
    	throw new RuntimeException(new OperationNotSupportedException());
    }

    /**
     * 38.
     * Stwórz mapę gdzie kluczem jest typ rachunku a wartością mapa mężczyzn posiadających ten rachunek, gdzie kluczem
     * jest obiekt User a wartością suma pieniędzy na rachunku danego typu przeliczona na złotkówki.
     */
    //TODO: zamiast Map<Stream<AccountType>, Map<User, BigDecimal>> metoda ma zwracać
    // Map<AccountType>, Map<User, BigDecimal>>, zweryfikować działania metody
    Map<Stream<AccountType>, Map<User, BigDecimal>> getMapWithAccountTypeKeyAndSumMoneyForManInPLN() {
    	throw new RuntimeException(new OperationNotSupportedException());
    }

    private Map<User, BigDecimal> manWithSumMoneyOnAccounts(final Company company) {
        return company
                .getUsers()
                .stream()
                .filter(isMan)
                .collect(Collectors.toMap(
                        Function.identity(),
                        this::getSumUserAmountInPLN
                ));
    }

    private BigDecimal getSumUserAmountInPLN(final User user) {
    	throw new RuntimeException(new OperationNotSupportedException());
    }

    /**
     * 39. Policz ile pieniędzy w złotówkach jest na kontach osób które nie są ani kobietą ani mężczyzną.
     */
    BigDecimal getSumMoneyOnAccountsForPeopleOtherInPLN() {
    	return allUsers()
    			.filter(Predicate.not(isMan))
    			.filter(Predicate.not(isWoman))
    			.flatMap(i -> i.getAccounts().stream())
    			.map(i -> getAccountAmountInPLN(i))
    			.reduce(BigDecimal::add)
    			.get()
    			.setScale(3);
    }

    /**
     * 40. Wymyśl treść polecenia i je zaimplementuj.
     * Policz ile osób pełnoletnich posiada rachunek oraz ile osób niepełnoletnich posiada rachunek. Zwróć mapę
     * przyjmując klucz True dla osób pełnoletnich i klucz False dla osób niepełnoletnich. Osoba pełnoletnia to osoba
     * która ma więcej lub równo 18 lat
     */
    Map<Boolean, Long> divideIntoAdultsAndNonAdults() {
    	throw new RuntimeException(new OperationNotSupportedException());
    }
}
